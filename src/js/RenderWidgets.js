'use strict'

let getData = function (httpMethods, path, cb) {
	let xhr;
	let callback = cb || function() {};
	xhr = new XMLHttpRequest();
	xhr.open(httpMethods, path);
	xhr.send();
	xhr.onload = function(evt) {
		let loadedData, rawData;
		rawData = evt.target.response;
		loadedData = JSON.parse(rawData);
		callback(loadedData);
	};
};

let container = document.querySelector('.rendered-widgets');
container.innerHTML = '';
let data = {};

let renderWidgets = function(dataToRender) {
	data.json = dataToRender;
	data.firstValueArr = 0;
	data.lastValueArr = 2;

	let fragment = document.createDocumentFragment();
	let i = 0;
	while(i !== data.lastValueArr + 1) {
			let element = document.createElement('div');
			element.className = 'widget-enter widget-enter-add';
			element.innerHTML = `<img class="img-enter img-enter-add" src="${data.json[i].link}">`;
			if (i + 1 > data.json.length - 1) { i = 0; } else { i ++; }
			fragment.appendChild(element);
			container.appendChild(fragment);
	}
	let nextBtn = document.querySelector('.next-btn');
	let prevBtn = document.querySelector('.prev-btn');
	nextBtn.addEventListener('click', nextImage);
	prevBtn.addEventListener('click', prevImage);
};

function timer(num, sec, foo) {
	let a = 0;
	return function () {
		a || window.setTimeout(function () {
			a = 0
		}, 1000 * sec);
		a < num && foo();
		a++;
	}
}

let lockBtn = false;

let nextImage = timer(1, 1, function() {
	if (lockBtn === false) {
		lockBtn = true;
		let div = document.createElement('div');
		if (data.lastValueArr + 1 > data.json.length - 1) { data.lastValueArr = 0; } else { data.lastValueArr = data.lastValueArr + 1; }
		if (data.firstValueArr + 1 > data.json.length - 1) { data.firstValueArr = 0; } else { data.firstValueArr = data.firstValueArr + 1; }
		div.className += "widget-enter widget-enter-add";
		div.innerHTML = `<img class="img-enter" src="${data.json[data.lastValueArr].link}">`;
		container.firstChild.firstChild.classList.remove('img-enter-add');
		container.firstChild.firstChild.classList.add('img-enter-leave');
		container.appendChild(div);
		setTimeout(function() {
			container.lastChild.firstChild.classList.add('img-enter-add');
			container.firstChild.classList.remove('widget-enter-add');
			container.firstChild.classList.add('widget-enter-leave');
			setTimeout(function() {
				container.firstChild.remove();
				lockBtn = false;
			}, 1000);
		}, 1)
	}
});

let prevImage = timer(1, 1, function() {
	if (lockBtn === false) {
		lockBtn = true;
		let div = document.createElement('div');
		if (data.lastValueArr - 1 < 0) { data.lastValueArr = data.json.length - 1; } else { data.lastValueArr = data.lastValueArr -1; }
		if (data.firstValueArr - 1 < 0) { data.firstValueArr = data.json.length - 1; } else { data.firstValueArr = data.firstValueArr - 1; }
		div.className += "widget-enter";
		div.innerHTML = `<img class="img-enter img-enter-leave" src="${data.json[data.firstValueArr].link}">`;
		container.lastChild.firstChild.classList.remove('img-enter-add');
		container.insertBefore(div, container.firstChild);
		setTimeout(function() {
			container.firstChild.firstChild.classList.add('img-enter-add');
			container.firstChild.firstChild.classList.remove('img-enter-leave');
			container.firstChild.classList.add('widget-enter-add');
			setTimeout(function() {
				container.lastChild.remove();
				lockBtn = false;
			}, 1000);
		}, 1)
	}
});

getData('GET', '../db/db.json', renderWidgets);