var nav = document.querySelector('.mobile-nav-open');
var navOpen = document.querySelector('.nav-enter');

openCloseNav = function() {
	if (navOpen.classList.value.indexOf('nav-enter-add') > 0) {
		navOpen.classList.remove('nav-enter-add');
		navOpen.parentNode.style.overflowY = '';
		navOpen.children[0].classList.remove('nav-list-enter-add');
	} else {
		navOpen.classList.add('nav-enter-add')
		navOpen.parentNode.style.overflowY = 'hidden';
		setTimeout(function() {navOpen.children[0].classList.add('nav-list-enter-add');}, 1000)
	}
};

startFunc = function() {
	nav.addEventListener('click', openCloseNav);
};



module.exports = startFunc;