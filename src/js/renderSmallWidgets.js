let smallWidgets = [ {}, {}, {}, {}, {}, {}, {}, {}, {} ];

function timer(num, sec, foo) {
	let a = 0;
	return function () {
		a || window.setTimeout(function () {
			a = 0
		}, 1000 * sec);
		a < num && foo();
		a++;
	}
}

let container = document.querySelector('.widgets-2 .images');

let q;

if (window.innerWidth > 1024) { q = 5 }
if (window.innerWidth > 768 && window.innerWidth <= 1024) { q = 3 }
if (window.innerWidth <= 768) { q = 2 }

let reRenderWidgets = timer(1, 0.2, function() {
	container.innerHTML = '';
	if (window.innerWidth > 1024) { q = 5 }
	if (window.innerWidth > 768 && window.innerWidth <= 1024) { q = 3 }
	if (window.innerWidth <= 768) { q = 2 }
	renderWidgets(smallWidgets)
});

window.addEventListener('resize', reRenderWidgets);

function renderWidgets(dataToRender) {
	let fragment = document.createDocumentFragment();
	let i = 0;
	while(i !== q) {
			let element = document.createElement('div');
			element.className = 'img';
			i ++;
			fragment.appendChild(element);
			container.appendChild(fragment);
	}
};

renderWidgets(smallWidgets);