(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
// Время в формате hh:mm:ss
var nowDate = new Date();
var nowHours, nowMinutes, nowSeconds;
var time = document.querySelector('.time');

var RenderTime = {};

RenderTime.nowTimeOnMyComputer = function() {
	nowDate.setTime(Date.parse(Date()));
	// Формат часов: hh
	if (String(nowDate.getHours()).length < 2) { nowHours =  0 + '' + nowDate.getHours(); } else { nowHours =  nowDate.getHours(); }
	// Формат минут: mm
	if (String(nowDate.getMinutes()).length < 2) { nowMinutes =  0 + '' + nowDate.getMinutes(); } else { nowMinutes =  nowDate.getMinutes(); }
	// Формат секунд: ss
	if (String(nowDate.getSeconds()).length < 2) { nowSeconds =  0 + '' + nowDate.getSeconds(); } else { nowSeconds =  nowDate.getSeconds(); }
	// Формат: hh:mm:ss
	time.innerText = nowHours + ':' + nowMinutes + ':' + nowSeconds;
	setTimeout(RenderTime.nowTimeOnMyComputer, 1000);
};

module.exports = RenderTime;
},{}],2:[function(require,module,exports){
'use strict'

let getData = function (httpMethods, path, cb) {
	let xhr;
	let callback = cb || function() {};
	xhr = new XMLHttpRequest();
	xhr.open(httpMethods, path);
	xhr.send();
	xhr.onload = function(evt) {
		let loadedData, rawData;
		rawData = evt.target.response;
		loadedData = JSON.parse(rawData);
		callback(loadedData);
	};
};

let container = document.querySelector('.rendered-widgets');
container.innerHTML = '';
let data = {};

let renderWidgets = function(dataToRender) {
	data.json = dataToRender;
	data.firstValueArr = 0;
	data.lastValueArr = 2;

	let fragment = document.createDocumentFragment();
	let i = 0;
	while(i !== data.lastValueArr + 1) {
			let element = document.createElement('div');
			element.className = 'widget-enter widget-enter-add';
			element.innerHTML = `<img class="img-enter img-enter-add" src="${data.json[i].link}">`;
			if (i + 1 > data.json.length - 1) { i = 0; } else { i ++; }
			fragment.appendChild(element);
			container.appendChild(fragment);
	}
	let nextBtn = document.querySelector('.next-btn');
	let prevBtn = document.querySelector('.prev-btn');
	nextBtn.addEventListener('click', nextImage);
	prevBtn.addEventListener('click', prevImage);
};

function timer(num, sec, foo) {
	let a = 0;
	return function () {
		a || window.setTimeout(function () {
			a = 0
		}, 1000 * sec);
		a < num && foo();
		a++;
	}
}

let lockBtn = false;

let nextImage = timer(1, 1, function() {
	if (lockBtn === false) {
		lockBtn = true;
		let div = document.createElement('div');
		if (data.lastValueArr + 1 > data.json.length - 1) { data.lastValueArr = 0; } else { data.lastValueArr = data.lastValueArr + 1; }
		if (data.firstValueArr + 1 > data.json.length - 1) { data.firstValueArr = 0; } else { data.firstValueArr = data.firstValueArr + 1; }
		div.className += "widget-enter widget-enter-add";
		div.innerHTML = `<img class="img-enter" src="${data.json[data.lastValueArr].link}">`;
		container.firstChild.firstChild.classList.remove('img-enter-add');
		container.firstChild.firstChild.classList.add('img-enter-leave');
		container.appendChild(div);
		setTimeout(function() {
			container.lastChild.firstChild.classList.add('img-enter-add');
			container.firstChild.classList.remove('widget-enter-add');
			container.firstChild.classList.add('widget-enter-leave');
			setTimeout(function() {
				container.firstChild.remove();
				lockBtn = false;
			}, 1000);
		}, 1)
	}
});

let prevImage = timer(1, 1, function() {
	if (lockBtn === false) {
		lockBtn = true;
		let div = document.createElement('div');
		if (data.lastValueArr - 1 < 0) { data.lastValueArr = data.json.length - 1; } else { data.lastValueArr = data.lastValueArr -1; }
		if (data.firstValueArr - 1 < 0) { data.firstValueArr = data.json.length - 1; } else { data.firstValueArr = data.firstValueArr - 1; }
		div.className += "widget-enter";
		div.innerHTML = `<img class="img-enter img-enter-leave" src="${data.json[data.firstValueArr].link}">`;
		container.lastChild.firstChild.classList.remove('img-enter-add');
		container.insertBefore(div, container.firstChild);
		setTimeout(function() {
			container.firstChild.firstChild.classList.add('img-enter-add');
			container.firstChild.firstChild.classList.remove('img-enter-leave');
			container.firstChild.classList.add('widget-enter-add');
			setTimeout(function() {
				container.lastChild.remove();
				lockBtn = false;
			}, 1000);
		}, 1)
	}
});

getData('GET', '../db/db.json', renderWidgets);
},{}],3:[function(require,module,exports){
var RenderTime = require('./RenderTime');
var RenderWidgets = require('./RenderWidgets');
var nav = require('./nav');
var smallWidgets = require('./renderSmallWidgets');

RenderTime.nowTimeOnMyComputer();
nav();
},{"./RenderTime":1,"./RenderWidgets":2,"./nav":4,"./renderSmallWidgets":5}],4:[function(require,module,exports){
var nav = document.querySelector('.mobile-nav-open');
var navOpen = document.querySelector('.nav-enter');

openCloseNav = function() {
	if (navOpen.classList.value.indexOf('nav-enter-add') > 0) {
		navOpen.classList.remove('nav-enter-add');
		navOpen.parentNode.style.overflowY = '';
		navOpen.children[0].classList.remove('nav-list-enter-add');
	} else {
		navOpen.classList.add('nav-enter-add')
		navOpen.parentNode.style.overflowY = 'hidden';
		setTimeout(function() {navOpen.children[0].classList.add('nav-list-enter-add');}, 1000)
	}
};

startFunc = function() {
	nav.addEventListener('click', openCloseNav);
};



module.exports = startFunc;
},{}],5:[function(require,module,exports){
let smallWidgets = [ {}, {}, {}, {}, {}, {}, {}, {}, {} ];

function timer(num, sec, foo) {
	let a = 0;
	return function () {
		a || window.setTimeout(function () {
			a = 0
		}, 1000 * sec);
		a < num && foo();
		a++;
	}
}

let container = document.querySelector('.widgets-2 .images');

let q;

if (window.innerWidth > 1024) { q = 5 }
if (window.innerWidth > 768 && window.innerWidth <= 1024) { q = 3 }
if (window.innerWidth <= 768) { q = 2 }

let reRenderWidgets = timer(1, 0.2, function() {
	container.innerHTML = '';
	if (window.innerWidth > 1024) { q = 5 }
	if (window.innerWidth > 768 && window.innerWidth <= 1024) { q = 3 }
	if (window.innerWidth <= 768) { q = 2 }
	renderWidgets(smallWidgets)
});

window.addEventListener('resize', reRenderWidgets);

function renderWidgets(dataToRender) {
	let fragment = document.createDocumentFragment();
	let i = 0;
	while(i !== q) {
			let element = document.createElement('div');
			element.className = 'img';
			i ++;
			fragment.appendChild(element);
			container.appendChild(fragment);
	}
};

renderWidgets(smallWidgets);
},{}]},{},[3])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvanMvUmVuZGVyVGltZS5qcyIsInNyYy9qcy9SZW5kZXJXaWRnZXRzLmpzIiwic3JjL2pzL21haW4uanMiLCJzcmMvanMvbmF2LmpzIiwic3JjL2pzL3JlbmRlclNtYWxsV2lkZ2V0cy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbkdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ05BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3JCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIi8vINCS0YDQtdC80Y8g0LIg0YTQvtGA0LzQsNGC0LUgaGg6bW06c3NcbnZhciBub3dEYXRlID0gbmV3IERhdGUoKTtcbnZhciBub3dIb3Vycywgbm93TWludXRlcywgbm93U2Vjb25kcztcbnZhciB0aW1lID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnRpbWUnKTtcblxudmFyIFJlbmRlclRpbWUgPSB7fTtcblxuUmVuZGVyVGltZS5ub3dUaW1lT25NeUNvbXB1dGVyID0gZnVuY3Rpb24oKSB7XG5cdG5vd0RhdGUuc2V0VGltZShEYXRlLnBhcnNlKERhdGUoKSkpO1xuXHQvLyDQpNC+0YDQvNCw0YIg0YfQsNGB0L7QsjogaGhcblx0aWYgKFN0cmluZyhub3dEYXRlLmdldEhvdXJzKCkpLmxlbmd0aCA8IDIpIHsgbm93SG91cnMgPSAgMCArICcnICsgbm93RGF0ZS5nZXRIb3VycygpOyB9IGVsc2UgeyBub3dIb3VycyA9ICBub3dEYXRlLmdldEhvdXJzKCk7IH1cblx0Ly8g0KTQvtGA0LzQsNGCINC80LjQvdGD0YI6IG1tXG5cdGlmIChTdHJpbmcobm93RGF0ZS5nZXRNaW51dGVzKCkpLmxlbmd0aCA8IDIpIHsgbm93TWludXRlcyA9ICAwICsgJycgKyBub3dEYXRlLmdldE1pbnV0ZXMoKTsgfSBlbHNlIHsgbm93TWludXRlcyA9ICBub3dEYXRlLmdldE1pbnV0ZXMoKTsgfVxuXHQvLyDQpNC+0YDQvNCw0YIg0YHQtdC60YPQvdC0OiBzc1xuXHRpZiAoU3RyaW5nKG5vd0RhdGUuZ2V0U2Vjb25kcygpKS5sZW5ndGggPCAyKSB7IG5vd1NlY29uZHMgPSAgMCArICcnICsgbm93RGF0ZS5nZXRTZWNvbmRzKCk7IH0gZWxzZSB7IG5vd1NlY29uZHMgPSAgbm93RGF0ZS5nZXRTZWNvbmRzKCk7IH1cblx0Ly8g0KTQvtGA0LzQsNGCOiBoaDptbTpzc1xuXHR0aW1lLmlubmVyVGV4dCA9IG5vd0hvdXJzICsgJzonICsgbm93TWludXRlcyArICc6JyArIG5vd1NlY29uZHM7XG5cdHNldFRpbWVvdXQoUmVuZGVyVGltZS5ub3dUaW1lT25NeUNvbXB1dGVyLCAxMDAwKTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gUmVuZGVyVGltZTsiLCIndXNlIHN0cmljdCdcblxubGV0IGdldERhdGEgPSBmdW5jdGlvbiAoaHR0cE1ldGhvZHMsIHBhdGgsIGNiKSB7XG5cdGxldCB4aHI7XG5cdGxldCBjYWxsYmFjayA9IGNiIHx8IGZ1bmN0aW9uKCkge307XG5cdHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuXHR4aHIub3BlbihodHRwTWV0aG9kcywgcGF0aCk7XG5cdHhoci5zZW5kKCk7XG5cdHhoci5vbmxvYWQgPSBmdW5jdGlvbihldnQpIHtcblx0XHRsZXQgbG9hZGVkRGF0YSwgcmF3RGF0YTtcblx0XHRyYXdEYXRhID0gZXZ0LnRhcmdldC5yZXNwb25zZTtcblx0XHRsb2FkZWREYXRhID0gSlNPTi5wYXJzZShyYXdEYXRhKTtcblx0XHRjYWxsYmFjayhsb2FkZWREYXRhKTtcblx0fTtcbn07XG5cbmxldCBjb250YWluZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucmVuZGVyZWQtd2lkZ2V0cycpO1xuY29udGFpbmVyLmlubmVySFRNTCA9ICcnO1xubGV0IGRhdGEgPSB7fTtcblxubGV0IHJlbmRlcldpZGdldHMgPSBmdW5jdGlvbihkYXRhVG9SZW5kZXIpIHtcblx0ZGF0YS5qc29uID0gZGF0YVRvUmVuZGVyO1xuXHRkYXRhLmZpcnN0VmFsdWVBcnIgPSAwO1xuXHRkYXRhLmxhc3RWYWx1ZUFyciA9IDI7XG5cblx0bGV0IGZyYWdtZW50ID0gZG9jdW1lbnQuY3JlYXRlRG9jdW1lbnRGcmFnbWVudCgpO1xuXHRsZXQgaSA9IDA7XG5cdHdoaWxlKGkgIT09IGRhdGEubGFzdFZhbHVlQXJyICsgMSkge1xuXHRcdFx0bGV0IGVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcblx0XHRcdGVsZW1lbnQuY2xhc3NOYW1lID0gJ3dpZGdldC1lbnRlciB3aWRnZXQtZW50ZXItYWRkJztcblx0XHRcdGVsZW1lbnQuaW5uZXJIVE1MID0gYDxpbWcgY2xhc3M9XCJpbWctZW50ZXIgaW1nLWVudGVyLWFkZFwiIHNyYz1cIiR7ZGF0YS5qc29uW2ldLmxpbmt9XCI+YDtcblx0XHRcdGlmIChpICsgMSA+IGRhdGEuanNvbi5sZW5ndGggLSAxKSB7IGkgPSAwOyB9IGVsc2UgeyBpICsrOyB9XG5cdFx0XHRmcmFnbWVudC5hcHBlbmRDaGlsZChlbGVtZW50KTtcblx0XHRcdGNvbnRhaW5lci5hcHBlbmRDaGlsZChmcmFnbWVudCk7XG5cdH1cblx0bGV0IG5leHRCdG4gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubmV4dC1idG4nKTtcblx0bGV0IHByZXZCdG4gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucHJldi1idG4nKTtcblx0bmV4dEJ0bi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIG5leHRJbWFnZSk7XG5cdHByZXZCdG4uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBwcmV2SW1hZ2UpO1xufTtcblxuZnVuY3Rpb24gdGltZXIobnVtLCBzZWMsIGZvbykge1xuXHRsZXQgYSA9IDA7XG5cdHJldHVybiBmdW5jdGlvbiAoKSB7XG5cdFx0YSB8fCB3aW5kb3cuc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG5cdFx0XHRhID0gMFxuXHRcdH0sIDEwMDAgKiBzZWMpO1xuXHRcdGEgPCBudW0gJiYgZm9vKCk7XG5cdFx0YSsrO1xuXHR9XG59XG5cbmxldCBsb2NrQnRuID0gZmFsc2U7XG5cbmxldCBuZXh0SW1hZ2UgPSB0aW1lcigxLCAxLCBmdW5jdGlvbigpIHtcblx0aWYgKGxvY2tCdG4gPT09IGZhbHNlKSB7XG5cdFx0bG9ja0J0biA9IHRydWU7XG5cdFx0bGV0IGRpdiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuXHRcdGlmIChkYXRhLmxhc3RWYWx1ZUFyciArIDEgPiBkYXRhLmpzb24ubGVuZ3RoIC0gMSkgeyBkYXRhLmxhc3RWYWx1ZUFyciA9IDA7IH0gZWxzZSB7IGRhdGEubGFzdFZhbHVlQXJyID0gZGF0YS5sYXN0VmFsdWVBcnIgKyAxOyB9XG5cdFx0aWYgKGRhdGEuZmlyc3RWYWx1ZUFyciArIDEgPiBkYXRhLmpzb24ubGVuZ3RoIC0gMSkgeyBkYXRhLmZpcnN0VmFsdWVBcnIgPSAwOyB9IGVsc2UgeyBkYXRhLmZpcnN0VmFsdWVBcnIgPSBkYXRhLmZpcnN0VmFsdWVBcnIgKyAxOyB9XG5cdFx0ZGl2LmNsYXNzTmFtZSArPSBcIndpZGdldC1lbnRlciB3aWRnZXQtZW50ZXItYWRkXCI7XG5cdFx0ZGl2LmlubmVySFRNTCA9IGA8aW1nIGNsYXNzPVwiaW1nLWVudGVyXCIgc3JjPVwiJHtkYXRhLmpzb25bZGF0YS5sYXN0VmFsdWVBcnJdLmxpbmt9XCI+YDtcblx0XHRjb250YWluZXIuZmlyc3RDaGlsZC5maXJzdENoaWxkLmNsYXNzTGlzdC5yZW1vdmUoJ2ltZy1lbnRlci1hZGQnKTtcblx0XHRjb250YWluZXIuZmlyc3RDaGlsZC5maXJzdENoaWxkLmNsYXNzTGlzdC5hZGQoJ2ltZy1lbnRlci1sZWF2ZScpO1xuXHRcdGNvbnRhaW5lci5hcHBlbmRDaGlsZChkaXYpO1xuXHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG5cdFx0XHRjb250YWluZXIubGFzdENoaWxkLmZpcnN0Q2hpbGQuY2xhc3NMaXN0LmFkZCgnaW1nLWVudGVyLWFkZCcpO1xuXHRcdFx0Y29udGFpbmVyLmZpcnN0Q2hpbGQuY2xhc3NMaXN0LnJlbW92ZSgnd2lkZ2V0LWVudGVyLWFkZCcpO1xuXHRcdFx0Y29udGFpbmVyLmZpcnN0Q2hpbGQuY2xhc3NMaXN0LmFkZCgnd2lkZ2V0LWVudGVyLWxlYXZlJyk7XG5cdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRjb250YWluZXIuZmlyc3RDaGlsZC5yZW1vdmUoKTtcblx0XHRcdFx0bG9ja0J0biA9IGZhbHNlO1xuXHRcdFx0fSwgMTAwMCk7XG5cdFx0fSwgMSlcblx0fVxufSk7XG5cbmxldCBwcmV2SW1hZ2UgPSB0aW1lcigxLCAxLCBmdW5jdGlvbigpIHtcblx0aWYgKGxvY2tCdG4gPT09IGZhbHNlKSB7XG5cdFx0bG9ja0J0biA9IHRydWU7XG5cdFx0bGV0IGRpdiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuXHRcdGlmIChkYXRhLmxhc3RWYWx1ZUFyciAtIDEgPCAwKSB7IGRhdGEubGFzdFZhbHVlQXJyID0gZGF0YS5qc29uLmxlbmd0aCAtIDE7IH0gZWxzZSB7IGRhdGEubGFzdFZhbHVlQXJyID0gZGF0YS5sYXN0VmFsdWVBcnIgLTE7IH1cblx0XHRpZiAoZGF0YS5maXJzdFZhbHVlQXJyIC0gMSA8IDApIHsgZGF0YS5maXJzdFZhbHVlQXJyID0gZGF0YS5qc29uLmxlbmd0aCAtIDE7IH0gZWxzZSB7IGRhdGEuZmlyc3RWYWx1ZUFyciA9IGRhdGEuZmlyc3RWYWx1ZUFyciAtIDE7IH1cblx0XHRkaXYuY2xhc3NOYW1lICs9IFwid2lkZ2V0LWVudGVyXCI7XG5cdFx0ZGl2LmlubmVySFRNTCA9IGA8aW1nIGNsYXNzPVwiaW1nLWVudGVyIGltZy1lbnRlci1sZWF2ZVwiIHNyYz1cIiR7ZGF0YS5qc29uW2RhdGEuZmlyc3RWYWx1ZUFycl0ubGlua31cIj5gO1xuXHRcdGNvbnRhaW5lci5sYXN0Q2hpbGQuZmlyc3RDaGlsZC5jbGFzc0xpc3QucmVtb3ZlKCdpbWctZW50ZXItYWRkJyk7XG5cdFx0Y29udGFpbmVyLmluc2VydEJlZm9yZShkaXYsIGNvbnRhaW5lci5maXJzdENoaWxkKTtcblx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuXHRcdFx0Y29udGFpbmVyLmZpcnN0Q2hpbGQuZmlyc3RDaGlsZC5jbGFzc0xpc3QuYWRkKCdpbWctZW50ZXItYWRkJyk7XG5cdFx0XHRjb250YWluZXIuZmlyc3RDaGlsZC5maXJzdENoaWxkLmNsYXNzTGlzdC5yZW1vdmUoJ2ltZy1lbnRlci1sZWF2ZScpO1xuXHRcdFx0Y29udGFpbmVyLmZpcnN0Q2hpbGQuY2xhc3NMaXN0LmFkZCgnd2lkZ2V0LWVudGVyLWFkZCcpO1xuXHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpIHtcblx0XHRcdFx0Y29udGFpbmVyLmxhc3RDaGlsZC5yZW1vdmUoKTtcblx0XHRcdFx0bG9ja0J0biA9IGZhbHNlO1xuXHRcdFx0fSwgMTAwMCk7XG5cdFx0fSwgMSlcblx0fVxufSk7XG5cbmdldERhdGEoJ0dFVCcsICcuLi9kYi9kYi5qc29uJywgcmVuZGVyV2lkZ2V0cyk7IiwidmFyIFJlbmRlclRpbWUgPSByZXF1aXJlKCcuL1JlbmRlclRpbWUnKTtcbnZhciBSZW5kZXJXaWRnZXRzID0gcmVxdWlyZSgnLi9SZW5kZXJXaWRnZXRzJyk7XG52YXIgbmF2ID0gcmVxdWlyZSgnLi9uYXYnKTtcbnZhciBzbWFsbFdpZGdldHMgPSByZXF1aXJlKCcuL3JlbmRlclNtYWxsV2lkZ2V0cycpO1xuXG5SZW5kZXJUaW1lLm5vd1RpbWVPbk15Q29tcHV0ZXIoKTtcbm5hdigpOyIsInZhciBuYXYgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubW9iaWxlLW5hdi1vcGVuJyk7XG52YXIgbmF2T3BlbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5uYXYtZW50ZXInKTtcblxub3BlbkNsb3NlTmF2ID0gZnVuY3Rpb24oKSB7XG5cdGlmIChuYXZPcGVuLmNsYXNzTGlzdC52YWx1ZS5pbmRleE9mKCduYXYtZW50ZXItYWRkJykgPiAwKSB7XG5cdFx0bmF2T3Blbi5jbGFzc0xpc3QucmVtb3ZlKCduYXYtZW50ZXItYWRkJyk7XG5cdFx0bmF2T3Blbi5wYXJlbnROb2RlLnN0eWxlLm92ZXJmbG93WSA9ICcnO1xuXHRcdG5hdk9wZW4uY2hpbGRyZW5bMF0uY2xhc3NMaXN0LnJlbW92ZSgnbmF2LWxpc3QtZW50ZXItYWRkJyk7XG5cdH0gZWxzZSB7XG5cdFx0bmF2T3Blbi5jbGFzc0xpc3QuYWRkKCduYXYtZW50ZXItYWRkJylcblx0XHRuYXZPcGVuLnBhcmVudE5vZGUuc3R5bGUub3ZlcmZsb3dZID0gJ2hpZGRlbic7XG5cdFx0c2V0VGltZW91dChmdW5jdGlvbigpIHtuYXZPcGVuLmNoaWxkcmVuWzBdLmNsYXNzTGlzdC5hZGQoJ25hdi1saXN0LWVudGVyLWFkZCcpO30sIDEwMDApXG5cdH1cbn07XG5cbnN0YXJ0RnVuYyA9IGZ1bmN0aW9uKCkge1xuXHRuYXYuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBvcGVuQ2xvc2VOYXYpO1xufTtcblxuXG5cbm1vZHVsZS5leHBvcnRzID0gc3RhcnRGdW5jOyIsImxldCBzbWFsbFdpZGdldHMgPSBbIHt9LCB7fSwge30sIHt9LCB7fSwge30sIHt9LCB7fSwge30gXTtcblxuZnVuY3Rpb24gdGltZXIobnVtLCBzZWMsIGZvbykge1xuXHRsZXQgYSA9IDA7XG5cdHJldHVybiBmdW5jdGlvbiAoKSB7XG5cdFx0YSB8fCB3aW5kb3cuc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG5cdFx0XHRhID0gMFxuXHRcdH0sIDEwMDAgKiBzZWMpO1xuXHRcdGEgPCBudW0gJiYgZm9vKCk7XG5cdFx0YSsrO1xuXHR9XG59XG5cbmxldCBjb250YWluZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcud2lkZ2V0cy0yIC5pbWFnZXMnKTtcblxubGV0IHE7XG5cbmlmICh3aW5kb3cuaW5uZXJXaWR0aCA+IDEwMjQpIHsgcSA9IDUgfVxuaWYgKHdpbmRvdy5pbm5lcldpZHRoID4gNzY4ICYmIHdpbmRvdy5pbm5lcldpZHRoIDw9IDEwMjQpIHsgcSA9IDMgfVxuaWYgKHdpbmRvdy5pbm5lcldpZHRoIDw9IDc2OCkgeyBxID0gMiB9XG5cbmxldCByZVJlbmRlcldpZGdldHMgPSB0aW1lcigxLCAwLjIsIGZ1bmN0aW9uKCkge1xuXHRjb250YWluZXIuaW5uZXJIVE1MID0gJyc7XG5cdGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+IDEwMjQpIHsgcSA9IDUgfVxuXHRpZiAod2luZG93LmlubmVyV2lkdGggPiA3NjggJiYgd2luZG93LmlubmVyV2lkdGggPD0gMTAyNCkgeyBxID0gMyB9XG5cdGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8PSA3NjgpIHsgcSA9IDIgfVxuXHRyZW5kZXJXaWRnZXRzKHNtYWxsV2lkZ2V0cylcbn0pO1xuXG53aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgcmVSZW5kZXJXaWRnZXRzKTtcblxuZnVuY3Rpb24gcmVuZGVyV2lkZ2V0cyhkYXRhVG9SZW5kZXIpIHtcblx0bGV0IGZyYWdtZW50ID0gZG9jdW1lbnQuY3JlYXRlRG9jdW1lbnRGcmFnbWVudCgpO1xuXHRsZXQgaSA9IDA7XG5cdHdoaWxlKGkgIT09IHEpIHtcblx0XHRcdGxldCBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG5cdFx0XHRlbGVtZW50LmNsYXNzTmFtZSA9ICdpbWcnO1xuXHRcdFx0aSArKztcblx0XHRcdGZyYWdtZW50LmFwcGVuZENoaWxkKGVsZW1lbnQpO1xuXHRcdFx0Y29udGFpbmVyLmFwcGVuZENoaWxkKGZyYWdtZW50KTtcblx0fVxufTtcblxucmVuZGVyV2lkZ2V0cyhzbWFsbFdpZGdldHMpOyJdfQ==
